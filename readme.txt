This folder contains three codes in python for the results presented in the paper "Assessment of medication adherence in respiratory diseases through 
deep sparse convolutional coding" presented at ETFA 2019 Conference.

"CNN architecture-training" contains the code for developing a cnn model and the procedure for training and testing the developed model.

"pruning with no retrain" contains the code for the first pruning technique. This exploits the fact that the parameters of the cnn model follow a normal
distribution with zero mean value. If the absolute value of a parameter is less than a given threshold is set to zero. The threshold is defined as the 
product of a decimal number (percentage) and the standard deviation of the parameters in each layer.

"pruning with retraing" contains the code for the second pruning method. In this case, for each pruned layer we retrain the rest network so that the 
layers that follow the pruned one adjust to the changes of the parameters.

In total five diffetent models are developed for the purposes of the paper:

Layers	        Model 1	          Model 2	         Model 3	         Model 4	         Model 5
Convolution	    16×4×4/ReLu       16x4x4/ReLu        8x4x4/ReLu          8x4x4/ReLu          16x4x4/ELU 
Max Pooling	    2×2	              2×2	             2×2	             2×2	             2×2
Dropout	        0.2	              0.2	             0.2	             0.2	             0.2
Convolution	    16×5×5/ReLu  	  16×5×5/ReLu  	     8×5×5/ReLu  	     8×5×5/ReLu  	     16×5×5/ELU 
Max Pooling	    2×2	              2×2	             2×2	             2×2	             2×2
Dropout	        0.1	              0.1	             0.1	             0.1	             0.1
Convolution	    16×6×6/ReLu  	  16×6×6/ReLu  	     8×6×6/ReLu  	     8×6×6/ReLu  	     16×6×6/ELU  
Max Pooling	    2×2	              2×2	             2×2	             2×2	             2×2
Dense	        64/ReLu	          64/ReLu	         64/ReLu	         64/ReLu	         64/ELU
Dense	        128/ReLu	      32/ReLu	         32/ReLu	         128/ReLu	         128/ELU
Dense	        64/ReLu	          16/ReLu	         16/ReLu	         64/ReLu	         64/ELU
Dense	        4/Softmax         4/Softmax	         4/Softmax	         4/Softmax	         4/Softmax
